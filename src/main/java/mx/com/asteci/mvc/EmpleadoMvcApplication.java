package mx.com.asteci.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpleadoMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpleadoMvcApplication.class, args);
	}

}
